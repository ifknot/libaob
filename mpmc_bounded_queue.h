#ifndef mpmc_bounded_queue_h
#define mpmc_bounded_queue_h

#include <queue>
#include <mutex>
#include <condition_variable>

namespace que {
 
    template <typename T, size_t capacity_>
    class mpmc_bounded_queue final {
		
        using lock_t = std::unique_lock<std::mutex>;

    public:

        using value_type = T;

        void wait_push(const T& e) {
            lock_t mlock(mutex_);
            not_full.wait(mlock, [this](){return queue_.size() < capacity_; });
            queue_.push(e);
            not_empty.notify_one();
		}
		
        void wait_push(T&& e) {
            lock_t mlock(mutex_);
            not_full.wait(mlock, [this](){return queue_.size() < capacity_; });
            queue_.push(std::move(e));
            not_empty.notify_one();
		}
		
        T wait_pop() {
            lock_t mlock(mutex_);
            not_empty.wait(mlock, [this](){return !queue_.empty(); });
            auto e = queue_.front();
            queue_.pop();
            not_full.notify_one();
            return e;
		}
		
        bool try_push(const T& e) {
            lock_t mlock(mutex_);
            if (queue_.size() == capacity_) return false;
            else {
                queue_.push(e);
                return true;
            }
		}
		
        bool try_push(T&& e) {
            lock_t mlock(mutex_);
            if (queue_.size() == capacity_) return false;
            else {
                queue_.push(e);
                return true;
            }
		}
		
        bool try_pop(T& e) {
            lock_t mlock(mutex_);
            if (queue_.empty()) return false;
            else {
                e = queue_.front();
                queue_.pop();
                return true;
            }
		}

        bool empty() {
            lock_t mlock(mutex_);
            return queue_.empty();
        }

        size_t size_guess() {
            lock_t mlock(mutex_);
            return queue_.size();
        }

        size_t capacity() {
            return capacity_;
        }

		
	private:

        std::mutex mutex_;
		std::condition_variable not_full;
		std::condition_variable not_empty;
        std::queue<T> queue_;
		
	};	

}

#endif
