#include <thread>

#include <bandit/bandit.h>

#include "mpmc_bounded_queue.h"
#include "scheduler.h"
#include "active_object.h"
#include "active_ostream.h"
#include "active_log.h"

using namespace bandit;

using queue_t = que::mpmc_bounded_queue<int,1024>;

void pusher(queue_t& q, size_t a, size_t b) {
    for(size_t i = a; i < b; ++i) q.wait_push(i);
}

void popper(queue_t& q, unsigned int a, unsigned int b, std::atomic<int>& sum) {
    for(size_t i = a; i < b; ++i) sum += q.wait_pop();
}

go_bandit([](){

    describe("test log", [](){

        describe("test queue", [](){

            queue_t Q;
            int N{1024};

            it("should be empty", [&](){
                AssertThat(Q.empty(), Equals(true));
            });

            it("should have default size 0", [&](){
                AssertThat(Q.size_guess(), Equals(0));
            });

            it("should be false try_pop()", [&](){
                int i;
                AssertThat(Q.try_pop(i), Equals(false));
            });

            it("should push and wait_pop 1 leaving an empty queue", [&](){
                Q.wait_push(1);
                int i = Q.wait_pop();
                AssertThat(i, Equals(1));
                AssertThat(Q.empty(), Equals(true));
            });

            it("should push N ints", [&](){
                for(int i = 0; i < N; ++i) Q.wait_push(i);
                AssertThat(Q.size_guess(), Equals(N));
            });

            it("should be non-empty", [&](){
                AssertThat(Q.empty(), Equals(false));
            });

            it("should try_pop drain", [&](){
                int i;
                int n = 0;
                while (Q.try_pop(i)) {
                    AssertThat(i, Equals(n++));
                }
                AssertThat(N, Equals(n));
            });

            it("should be empty", [&](){
                AssertThat(Q.empty(), Equals(true));
            });

            it("should wait_push N ints", [&](){
                for(int i = 0; i < N; ++i) Q.wait_push(i);
                AssertThat(Q.size_guess(), Equals(N));
            });

            it("should be non-empty", [&](){
                AssertThat(Q.empty(), Equals(false));
            });

            it("should wait_pop to empty in order enqueued", [&](){
                for(int i = 0; i < N; ++i) {
                    AssertThat(Q.wait_pop(), Equals(i));
                }
            });

            it("should be empty", [&](){
                AssertThat(Q.empty(), Equals(true));
            });

            it("should n(n+1)/2 multithread wait_pop thrash", [&](){
                std::vector<std::thread> threads;
                size_t N = 1023;
                size_t q_size = N + 1;
                size_t pushers = 4;
                size_t poppers = 4;
                std::atomic<int> sum{0};
                for(size_t i = 0; i < poppers; ++i) {
                    threads.emplace_back(std::thread(popper, std::ref(Q), i * (q_size / poppers), (i + 1) * (q_size / poppers), std::ref(sum)));
                }
                for(size_t i = 0; i < pushers; ++i) {
                    threads.emplace_back(std::thread(pusher, std::ref(Q), i * (q_size / pushers), (i + 1) * (q_size / pushers)));
                }

                for(auto& t : threads) t.join();
                AssertThat(sum.load(), Equals((N * (N + 1) / 2)));
            });

        });

        describe("test active object", [](){

            using msgq_t = que::mpmc_bounded_queue<aob::message,1024>;
            using sched_t = aob::scheduler<msgq_t>;

            msgq_t log;
            sched_t sched(log);
            aob::active_object<1024> obj;
            int i{0};

            it("should log i == 2 ", [&](){
                log.wait_push([&i]{
                    i++;
                    std::cout << std::this_thread::get_id() << " i =";
                });
                log.wait_push([&i]{
                    i++;
                    std::cout << "= " << i;
                });
                while(!log.empty()) {}
                AssertThat(i, Equals(2));
            });

            it("should send i == 0", [&](){
                obj.send([&i]{
                   --i;
                   std::cout << std::this_thread::get_id() << " i =";
                });
                obj.send([&i]{
                    --i;
                    std::cout << "= " << i;
                });
                while(obj.busy()) {}
                AssertThat(i, Equals(0));
            });

        });

        describe("test active stream", [](){

            std::ofstream f("test.txt");
            aob::active_ostream<1024> aout(f);

            it("", [&](){
                aout << "hello a number =" << 100;
                //AssertThat(, Equals());
            });

        });

        describe("test active log", [](){

            log::active_log trace("test.log");

            it("", [&](){

                //AssertThat(, Equals());
            });

        });

        /*
        describe("", [](){

            it("", [&](){

                //AssertThat(, Equals());
            });

        });
        */

    });

});
