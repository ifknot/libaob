#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <thread>

namespace aob {

    using message = std::function<void()>;

    template<typename T>
    class scheduler {

    public:

        scheduler(T& q):
            q(q),
            t(std::unique_ptr<std::thread>(new std::thread([this] {
                this->run();
            }))) {}

        ~scheduler() {
            done = true;
            t->join();
        }

    private:

        void run() {
            message m;
            while(!done) { // busy wait for messages
                if(q.try_pop(m)) m();
            }
            while(!q.empty()) { // finish remaining messages
                if(q.try_pop(m)) m();
            }
        }

        bool done = false;
        T& q;
        std::unique_ptr<std::thread> t;

        scheduler(const scheduler&) = delete;

        void operator=(const scheduler&) = delete;

    };

}

#endif // SCHEDULER_H
