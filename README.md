# README #

![run dmc](https://cldup.com/hvWPnVyYCR.jpg)

### Why libaob? ###

*Because [As Herb Sutter said...](http://www.drdobbs.com/parallel/use-threads-correctly-isolation-asynch/215900465): >"Use Threads Correctly = Isolation + Asynchronous Messages"

[Wikipedia](https://en.wikipedia.org/wiki/Active_object)The active object design pattern decouples method execution from method invocation for objects that each reside in their own thread of control.[1] The goal is to introduce concurrency, by using asynchronous method invocation and a scheduler for handling requests.[2]

[1] Douglas C. Schmidt; Michael Stal; Hans Rohnert; Frank Buschmann (2000). Pattern-Oriented Software Architecture, Volume 2: Patterns for Concurrent and Networked Objects. John Wiley & Sons. ISBN 0-471-60695-2.
[2] Jump up ^ Bass, L., Clements, P., Kazman, R. Software Architecture in Practice. Addison Wesley, 2003

### C++ Active Objects? ###

Based on Herb Sutter's article [Prefer Using Active Objects Instead of Naked Threads](http://www.drdobbs.com/parallel/prefer-using-active-objects-instead-of-n/225700095)

### Licence ###
`libaob`is free (as in beer) software and is licensed under the [GNU Lesser General Public License (LGPL) version 3](https://www.gnu.org/licenses/lgpl-3.0.en.html).

### Contribution guidelines ###

* __Writing tests__ All tests are welcome _but_ must be submitted in the [Bandit](http://banditcpp.org/) unit testing framework format.
* __Code review__ All _constructive_ code review is welcome

### Who do I talk to? ###

* Repo owner: https://bitbucket.org/ifknot/
* Support: [@ifknot](https://twitter.com/ifknot)
