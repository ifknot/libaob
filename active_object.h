#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_H

#include "mpmc_bounded_queue.h"
#include "scheduler.h"

namespace aob {

    template<size_t capacity>
    class active_object {

        using queue_t = que::mpmc_bounded_queue<aob::message, capacity>;

    public:

        active_object(): s(q) {}

        void send(message m) {
            q.wait_push(m);
        }

        bool busy()  {
            return !q.empty();
        }

        virtual ~active_object() = default;

    private:

        scheduler<queue_t> s;
        queue_t q;

        active_object(const active_object&) = delete;

        void operator=( const active_object& ) = delete;

    };

}

#endif // ACTIVE_OBJECT_H
